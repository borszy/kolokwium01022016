Szcz�liwych chwil, szcz�liwych dni
Edgar Allan Poe

t�umaczenie:  W�odzimierz Lewik 

Szcz�liwych chwil, szcz�liwych dni
Zazna�o serce me rozdarte...
Sny o pot�dze, dumne sny
Min�y z wiatrem.

Sny o pot�dze? �ni�em tak...
Jak z�rz znikn�y z�ote pasmo.
M�odo�ci wizje gasn� wszak...
Niech sobie gasn�.

A duma? C� po tobie, c�?
Twoj� trucizn� i katusz�
Kto� inny truje si� z mych kru�...
O, zamilcz, duszo!

Te szcz�sne chwile, szcz�sne dni
Widzia�em - jak�e oku mi�e...
Sny o pot�dze, dumne sny
Ju� raz prze�ni�em.

Lecz gdyby nawet chwil tych blask
I dawne m�ki zn�w wr�ci�y -
Nie, nie! Tak cierpie� jeszcze raz
Nie mia�bym si�y.

Bo na ich skrzydle czarny cie�...
A gdy to skrzyd�o za�opoce
Trucizna w dusz� sp�ywa ze� -
W dusz�, co zna jej moce.
