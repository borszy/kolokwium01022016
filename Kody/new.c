int main(){
	int n;
	scanf("%d", &n);
	int x;
	int sum = 0;
	while(n--){
		scanf("%d", &x);
		sum+=x;
	}
	printf("Suma: %d\n", sum);
	return 0;
}